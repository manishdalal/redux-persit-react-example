import React, { PureComponent } from "react";
import { connect } from "react-redux";

// import logo from "../logo.svg";
import "./home.css";
// import APICaller from "../utils/APICaller";
import { getData } from "../redux/actions/user";
// import { Link } from "react-router-dom";

class HomePage extends PureComponent {
  componentDidMount() {
    // const result = APICaller()
    //   .then(res => {
    //     console.log("success", ">>>>>>>>>>>>");
    //   })
    //   .catch(err => {
    //     console.log("error", err);
    //   });

    console.log("didmount", "HomePage");

    this.props.dispatch(getData());
  }

  componentWillUnmount() {
    console.log("componentUnmount", "HomePage");
  }

  onClick = () => {
    this.props.dispatch(getData());
  };
  prettifyJson = jsonObj =>
    Object.keys(jsonObj).map(key => {
      const value = jsonObj[key];
      let valueType = typeof value;
      const isSimpleValue =
        ["string", "number", "boolean"].includes(valueType) || !value;
      if (isSimpleValue && valueType === "object") {
        valueType = "null";
      }
      return (
        <div key={key} className="line">
          <span className="key">{key}:</span>
          {isSimpleValue ? (
            <span className={valueType}>{`${value}`}</span>
          ) : (
            this.prettifyJson(value)
          )}
        </div>
      );
    });
  render() {
    console.log("data in render", this.props.data);
    return (
      <div className="App">
        <button onClick={this.onClick}>Get data</button>
        {this.props.data &&
          this.props.data.map((row, index) => {
            return (
              <div className="box" key={index.toString()}>
                {this.prettifyJson(row)}
                {/* <h6>{row.isbn}</h6>
              <h4>{row.title ? row.title : ""}</h4>
              <h5>{row.country}</h5>
              <h5>{row.imageLink}</h5>
              <h5>{row.link}</h5>
              <h5>{row.pages}</h5>
              <h5>{row.year}</h5> */}
              </div>
            );
          })}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state.user.data,
    ApiInProgress: state.user.ApiInProgress
  };
}

export default connect(mapStateToProps)(HomePage);
