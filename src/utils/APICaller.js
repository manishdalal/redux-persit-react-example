import axios from "axios";
// import get from "lodash/get";

// import store from "../redux";
// import { logout } from "../redux/actions/user";

const APICaller = (
  method = "get",
  reqUrl,
  data = {},
  headers = {
    "content-type": "application/json",
    Accept: "application/json"
  }
) => {
  return new Promise(async (resolve, reject) => {
    let url = "https://jsonplaceholder.typicode.com/comments";
    // token = token && !loginRoutes.includes(data.id) ? { token } : {};
    // data = { ...data, ...token, timezoneOffset: "-330", platform: Platform.OS };
    // console.log(url, "data", data, "method", method, "header", headers);

    let options = {
      method,
      url,
      data,
      headers
    };
    // if (method.toLowerCase() === "get") delete options["data"];
    axios({ ...options })
      .then(response => {
        console.log("%c{res}", "color: green", " => ", response); // eslint-disable-line no-console
        resolve(response);
      })
      .catch(error => {
        // let message = get(error, "response.data.response.error.message", "");
        // let code = get(error, "response.data.code", "");
        // if (code === 500 && message.includes("Invalid token")) {
        //   store.dispatch(logout());
        // }
        console.log("%c{err}", "color: red", ` => [${url} >>`, error.response); // eslint-disable-line no-console
        reject(error);
      });
  });
};
export default APICaller;
