import React from "react";
import { Provider } from "react-redux";
import { Switch, Route } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import store, { persistor } from "./redux";

import HomePage from "./HomePage/homePage";

class App extends React.Component {
  render() {
    return (
      <Provider store={store} style={{ flex: 1 }}>
        <PersistGate loading={null} persistor={persistor}>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/homeDetail" component={HomePage} />
          </Switch>
        </PersistGate>
      </Provider>
    );
  }
}
export default App;
