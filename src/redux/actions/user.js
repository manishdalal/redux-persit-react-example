import ActionTypes from "../constants";

export const logout = () => ({
  type: ActionTypes.USER_LOGOUT
});

export const saveUser = payload => ({
  type: ActionTypes.SAVE_USER,
  payload
});

export const getData = () => ({
  type: ActionTypes.GET_DATA
});
