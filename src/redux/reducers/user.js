import ActionTypes from "../constants";
import findIndex from "lodash/findIndex";

const initialState = {
  LogoutInProgress: false,
  ApiInProgress: false,
  userProfile: {},
  token: "",
  data: []
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.GET_DATA:
      console.log("GET_DATA", "reducer called");
      return Object.assign({}, state, {
        ApiInProgress: true
      });

    case ActionTypes.GET_DATA_FAILURE:
      console.log("GET_DATA_FAILURE", "reducer called");
      return Object.assign({}, state, {
        ApiInProgress: false
      });
    case ActionTypes.SAVE_DATA:
      console.log("SAVE_DATA", "reducer called");
      const newData = [];
      action.payload.forEach(element => {
        const key = element.isbn ? "isbn" : "id";
        if (element[key]) {
          const index = findIndex(state.data, o => o[key] === element[key]);
          if (index !== -1) {
            state.data[index] = element;
          } else {
            newData.push(element);
          }
        }
      });
      return {
        ...state,
        data: [...state.data, ...newData],
        ApiInProgress: false
      };
    case ActionTypes.SAVE_USER:
      return Object.assign({}, state, {
        userProfile: action.payload.userProfile,
        token: action.payload.access_token
      });
    case ActionTypes.USER_LOGOUT:
      // Storage.remove("token");
      // Storage.remove("userProfile");
      // Storage.remove("fcmToken");
      return Object.assign({}, state, {});

    default:
      return state;
  }
};

export default user;
