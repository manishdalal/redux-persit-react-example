import { all, call, put, takeLatest } from "redux-saga/effects";
// import { select } from "redux-saga/effects";
import ActionTypes from "../constants/index";
import APICaller from "../../utils/APICaller";

export function* login(payload) {
  try {
    const response = yield call(
      APICaller,
      "Account/login",
      "POST",
      payload.payload
    );
    yield put({
      type: ActionTypes.USER_LOGIN_SUCCESS,
      payload: response.data
    });
  } catch (err) {
    yield put({
      type: ActionTypes.USER_LOGIN_FAILURE,
      payload: err.data
    });
  }
}

export function* getData(payload) {
  try {
    const response = yield call(APICaller);
    console.log("api response", response);
    yield put({
      type: ActionTypes.SAVE_DATA,
      payload: response.data
    });
  } catch (err) {
    yield put({
      type: ActionTypes.GET_DATA_FAILURE,
      payload: err
    });
  }
}

export default function* root() {
  yield all([
    takeLatest(ActionTypes.USER_LOGIN, login),
    takeLatest(ActionTypes.GET_DATA, getData)
  ]);
}
